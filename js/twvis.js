class InterGraph {
	constructor(nodes, edges, index, maxRedSoFar, theseContractions) {
		var _self = this;
		this.index = index;
		this.nodes = nodes;
		this.edges = edges;
		if (theseContractions) {
			this.contractions = theseContractions;
		}
		const [graphCont, contractButton, reddegdiv, optimalButton] = createRow(this.index+1);
		this.textDiv = reddegdiv;
		this.optimalButton = optimalButton;
		var degrees = {};
		nodes.forEach(function(node) {
			degrees[node.id] = 0;
		});
		edges.forEach(function(edge) {
			if (edge.color == 'red') {
				degrees[edge.from] += 1;
				degrees[edge.to] += 1;
			}
		});
		var maxDegree = 0;
		for( var k in degrees) {
			var d = degrees[k];
			if (d > maxDegree) {
				maxDegree = d;
			}
		}
		this.container = graphCont;
		this.contractButton = contractButton;
		var myoptions = {
			autoResize: true,
			manipulation: {
			},
			interaction: {
				multiselect: true,
				selectConnectedEdges: false
			}
		};
		if (this.index == 0) {
			myoptions.manipulation = {
				enabled: true,
				addNode: function(nodeData, callback) {
					var ind = _self.network.body.nodeIndices.length + 1;
					nodeData.label = 'v' + ind;
					callback(nodeData);
				},
				addEdge: function(edgeData, callback) {
					edgeData.color = 'black';
					edgeData.width = 3;
					callback(edgeData);
					_self.updateEdgeListURI();
				}
			};
		} else {
			myoptions.manipulation = {
				enabled: false
			}
		}
		this.network = new vis.Network(
			graphCont,
			{ nodes: this.nodes, edges: this.edges },
			myoptions
		);
		this.network.on("select", function(e) {
			_self.updateEdgeListURI();
		});
		this.network.on("click", function(e) {
			if (_self.network.getSelectedNodes().length == 2 && _self.network.getSelectedEdges().length == 0) {
				_self.contractButton.disabled = false;
			} else {
				_self.contractButton.disabled = true;
			}
		});
		if (index == 0) {
			this.biggestSoFar = 0;
			this.textDiv.innerText = '';
			optimalButton.onclick = function(e) { requestOptimal(_self) };
		} else {
			if (maxDegree >= maxRedSoFar) {
				this.biggestSoFar = maxDegree;
				reddegdiv.innerText = 'Max red degree here is ' + maxDegree + ' and this is largest so far';
			} else {
				this.biggestSoFar = maxRedSoFar;
				reddegdiv.innerText = 'Max red degree here is ' + maxDegree + ' but largest so far is ' + maxRedSoFar;
			}
		}
		this.contractButton.onclick = function(e) {
			// Get the graph
			var edges = _self.network.body.data.edges.getDataSet();
			var nodes = _self.network.body.data.nodes.getDataSet();
			var contracted = _self.network.getSelectedNodes();
			var newNodes = [];
			let positions = _self.network.getPositions();
			nodes.forEach(function(node) {
				if (!contracted.includes(node.id)) {
					newNodes.push({id: node.id, label: node.label, x: positions[node.id].x, y: positions[node.id].y});
				}
			});
			// TODO Will this be the right ID?#
			var contractId = nodes.get(contracted[0]).id;
			let newX = (positions[contracted[0]].x + positions[contracted[1]].x)/2;
			let newY = (positions[contracted[0]].y + positions[contracted[1]].y)/2;
			newNodes.push({id: contractId,
				label: nodes.get(contracted[0]).label + '|' + nodes.get(contracted[1]).label,
				x: newX,
				y: newY
			});
			// Add edges
			var newEdges = [];
			var handledNodes = [];
			edges.forEach(function(edge) {
				if (!contracted.includes(edge.from) && !contracted.includes(edge.to)) {
					newEdges.push({from: edge.from, to: edge.to, color: edge.color, width: 3});
				} else if (contracted.includes(edge.from) && contracted.includes(edge.to)) {
					// Edge is between constracted vertices, ignore it.
				} else {
					// Edge incident to one contracted vertex. Make sure we map it to the
					// contracted vertex
					var color = edge.color;
					var otherContracted = contracted[0];
					if (contracted.includes(edge.from)) {
						if (!handledNodes.includes(edge.to)) {
							if (otherContracted == edge.from) {
								otherContracted = contracted[1];
							}
							var otherEdges = _self.network.getConnectedEdges(otherContracted).filter(function(otherEdgeId) {
								var otherEdge = edges.get(otherEdgeId);
								return (edge.to == otherEdge.from) || (edge.to == otherEdge.to);
							});
							if ((otherEdges.length == 0) || (edges.get(otherEdges[0]).color == 'red')) {
								color = 'red';
							}
							newEdges.push({from: contractId, to: edge.to, color: color, width: 3});
							handledNodes.push(edge.to);
						}
					} else {
						if (!handledNodes.includes(edge.from)) {
							if (otherContracted == edge.to) {
								otherContracted = contracted[1];
							}
							var otherEdges = _self.network.getConnectedEdges(otherContracted).filter(function(otherEdgeId) {
								var otherEdge = edges.get(otherEdgeId);
								return (edge.from == otherEdge.from) || (edge.from == otherEdge.to);
							});
							if ((otherEdges.length == 0) || (edges.get(otherEdges[0]).color == 'red')) {
								color = 'red';
							}
							newEdges.push({from: edge.from, to: contractId, color: color, width: 3});
							handledNodes.push(edge.from);
						}
					}
				}
			});
			// Create new InterGraph thing
			var nextContractions = null;
			if (_self.contractions && _self.contractions.length > 1) {
				nextContractions = _self.contractions.slice(1);
			}
			var nextGraph = new InterGraph(newNodes, newEdges, _self.index + 1, _self.biggestSoFar, nextContractions);
		}
		if (theseContractions) {
			this.applyContractions();
		}
		this.network.setSize(this.container.scrollWidth, this.container.scrollHeight);
		if (!this.contractions || this.contractions.length == 0) {
			graphCont.scrollIntoView();
		}
	}

	edgeListStr() {
		var str = "[";
		var edges = this.network.body.data.edges.getDataSet();
		var nodes = this.network.body.data.nodes.getDataSet();
		edges.forEach(function(edge) {
			let va = nodes.get(edge.from).label.substring(1);
			let vb = nodes.get(edge.to).label.substring(1);
			str += "[" + va +  "," + vb + "],";
		});
		str = str.substring(0, str.length-1) + "]";
		return str;
	}

	updateEdgeListURI() {
		var params = new URLSearchParams(location.search);
		params.set("edgeList", this.edgeListStr());
		window.history.replaceState({}, "", "index.html?" + params.toString());
	}

	applyContractions() {
		if (this.contractions.length > 0) {
			let [a, b] = this.contractions[0];
			var nodes = this.network.body.data.nodes.getDataSet();
			var nodeA;
			var nodeB;
			nodes.forEach(function(node) {
				const rea = new RegExp("v" + a + "(,|$)");
				const reb = new RegExp("v" + b + "(,|$)");
				if (rea.test(node.label)) {
					nodeA = node.id;
				}
				if (reb.test(node.label)) {
					nodeB = node.id;
				}
			});
			this.network.setSelection({nodes: [nodeA, nodeB], edges: []});
			this.contractButton.disabled = false;
			this.contractButton.click();
		}
	}
}

async function getOptimal(graphitem) {
	let edgeStr = graphitem.edgeListStr();
	const response = await fetch("https://mc.ewpettersson.se/tww?edgeList=" + edgeStr);
	const jsonData = await response.json();
	return jsonData
};

function requestOptimal(_self) {
	_self.optimalButton.disable = true;
	_self.optimalButton.innerText = 'Calculating';
	getOptimal(_self).then((data) => {
		if ('Error' in data) {
			_self.textDiv.innerText = 'Error: ' + data['error'];
			_self.textDiv.classList.add('alert-warning');
			_self.optimalButton.disable = false;
			_self.optimalButton.innerText = 'Find optimal contraction';
		} else {
			_self.contractions = data['contractions'].map(function(pair) { return pair.split(","); });
			let contString = '[' + data['contractions'].join('], [') + ']';
			_self.textDiv.innerText = 'Twin-width is ' + data['tww'] + ' and an optimal contraction sequence is ' + contString;
			_self.textDiv.classList.add('alert-success');
			_self.optimalButton.innerText = 'Apply optimal contractions';
			_self.optimalButton.onclick = function(e) { applyContractions(_self) };
		}
	});
}

function applyContractions(_self) {
	_self.applyContractions();
}

let params = new URLSearchParams(location.search)
// example of retrieving 'id' parameter
let edgeList = params.get("edgeList");
var nodesAdded = {};
var nodes = [];
var edges = [];
if (edgeList && edgeList.length > 2) {
	try {
		let edgesStr = edgeList.substring(2, edgeList.length - 2).split("],[")
		edgesStr.forEach(function (edge) {
			let [a, b] = edge.split(",").map(function (o) { return parseInt(o);});
			if (! (a in nodesAdded) ) {
				nodesAdded[a] = true;
				nodes.push({id: a, label: "v" + a});
			}
			if (! (b in nodesAdded) ) {
				nodesAdded[b] = true;
				nodes.push({id: b, label: "v" + b});
			}
			edges.push({from: a, to: b, color: 'black', width: 3});
		});
	} catch (e) {
	} // Do nothing, probably missing or malformed input
}
if (edges.length == 0) {
	nodes = [
		{ id: 1, label: "v1" },
		{ id: 2, label: "v2" },
		{ id: 3, label: "v3" },
		{ id: 4, label: "v4" },
		{ id: 5, label: "v5" },
	];

	// create an array with edges
	edges = [
		{ from: 1, to: 3, color: 'black', width: 3},
		{ from: 1, to: 2, color: 'black', width: 3},
		{ from: 2, to: 4, color: 'black', width: 3},
		{ from: 2, to: 5, color: 'black', width: 3},
	];
}
var nodes = new vis.DataSet(nodes);
var edges = new vis.DataSet(edges);

var mainGraph = new InterGraph(nodes, edges, 0, 0);


function createRow(index) {
	// Delete the row, if it exists, and anything after it
	var laterRowId = index;
	var laterRow;
	while((laterRow = document.querySelector("#row-" + laterRowId))) {
		laterRow.remove();
		laterRowId += 1;
	}

	// Create new row
	var contractButton = document.createElement("button");
	contractButton.type = "button";
	contractButton.classList.add("btn", "btn-primary", "btn-contract");
	contractButton.disabled = true;
	contractButton.innerText = "Contract two vertices";
	var cBc = document.createElement("div");
	cBc.appendChild(contractButton);
	var cBr = document.createElement("div");
	cBr.appendChild(cBc);
	if (index == 1) {
		var optimalButton = document.createElement("button");
		optimalButton.type = "button";
		optimalButton.classList.add("btn", "btn-primary");
		optimalButton.innerText = "Find optimal contraction";
		var oBr = document.createElement("row");
		oBr.appendChild(optimalButton);
		var rDd = document.createElement("div");
		rDd.classList.add("alert");
		oBr.appendChild(rDd);
	} else {
		var rDd = document.createElement("div");
		rDd.classList.add("reddeg");
		rDd.innerText = "Red degree: 0";
		var rDr = document.createElement("row");
		rDr.appendChild(rDd);
	}
	var bCon = document.createElement("div");
	bCon.classList.add("container");
	bCon.appendChild(cBr);
	if (index == 1) {
		bCon.appendChild(oBr);
	} else {
		bCon.appendChild(rDr);
	}
	var bCol = document.createElement("div");
	bCol.classList.add("col-3");
	bCol.appendChild(bCon);
	var graphCont = document.createElement("div");
	graphCont.id = "graph-" + index;
	graphCont.classList.add("graph");
	var graphCol = document.createElement("div");
	graphCol.classList.add("col-9");
	graphCol.appendChild(graphCont);
	var row = document.createElement("div");
	row.classList.add("row");
	row.id = "row-" + index;
	row.appendChild(graphCol);
	row.appendChild(bCol);
	var mainContainer = document.querySelector("#graphContainer");
	mainContainer.appendChild(row);
	return [graphCont, contractButton, rDd, optimalButton];
}
