class InterGraph {
	constructor(nodes, edges) {
		this.nodes = nodes;
		this.edges = edges;
	}
}


var nodes = new vis.DataSet([
  { id: 1, label: "1" },
  { id: 2, label: "2" },
  { id: 3, label: "3" },
  { id: 4, label: "4" },
  { id: 5, label: "5" },
]);

// create an array with edges
var edges = new vis.DataSet([
  { from: 1, to: 3 },
  { from: 1, to: 2 },
  { from: 2, to: 4 },
  { from: 2, to: 5 },
]);

var mainGraph = {};
mainGraph.network = new vis.Network(
	document.getElementById("mainGraph"),
	data = { nodes: nodes, edges: edges },
	options = {
		manipulation: {
			enabled: true,
			addNode: function(nodeData, callback) {
				var ind = 0;
				for(var i = 0; i < mainGraph.network.body.nodeIndices.length; ++i) {
					// get lowest integer not yet used
				}
				nodeData.label = '' + ind;
				callback(nodeData);
			}
		},
		interaction: {
			multiselect: true,
			selectConnectedEdges: false
		}
	};
);
mainGraph.container = document.querySelector("#graphContainer");
mainGraph.contractButton = mainGraph.container.querySelector("button.btn-contract");
mainGraph.network.on("select", function(e) {
	if (mainGraph.network.getSelectedNodes().length == 2 && mainGraph.network.getSelectedEdges().length == 0) {
		mainGraph.contractButton.disabled = false;
	} else {
		mainGraph.contractButton.disabled = true;
	}
});
mainGraph.contractButton.onclick = function(e) {
	// Get the graph
	var edges = mainGraph.network.body.data.edges.getDataSet();
	var nodes = mainGraph.network.body.data.nodes.getDataSet();
	var contracted = mainGraph.network.getSelectedNodes();
	for (var node in nodes) {

	}
	var newNodes = nodes.get({
		filter: function(item) {
			return !contracted.includes(item);
		}
	});
	// TODO Will this be the right ID?
	newNodes.add({id: contracted[0].id,
		label: contracted[0].label + '|' + contracted[1].label
	});
	// Add edges back in
	// for each old-edge
	// if not incident to either contracted, just add it
	// else ... blah

	// create new things
	createRow(index+1, nodes, edges);
}


function createRow(index) {
	// Delete the row, if it exists, and anything after it
	var laterRowId = index;
	var laterRow;
	while((laterRow = document.querySelector("#row-" + laterRowId))) {
		laterRow.remove();
		console.log("deleted " + "#row-" + laterRowId);
		laterRowId += 1;
	}

	// Create new row
	var contractButton = document.createElement("button");
	contractButton.type = "button";
	contractButton.classList.add("btn", "btn-primary", "btn-contract");
	contractButton.disabled = true;
	contractButton.innerText = "Contract two vertices";
	var cBc = document.createElement("div");
	cBc.appendChild(contractButton);
	var cBr = document.createElement("div");
	cBr.appendChild(cBc);
	var optimalButton = document.createElement("button");
	optimalButton.type = "button";
	optimalButton.classList.add("btn", "btn-primary");
	optimalButton.innerText = "Find optimal contraction";
	var oBc = document.createElement("div");
	oBc.appendChild(optimalButton);
	var oBr = document.createElement("row");
	oBr.appendChild(oBc);
	var bCon = document.createElement("div");
	bCon.classList.add("container");
	bCon.appendChild(cBr);
	bCon.appendChild(oBr);
	var bCol = document.createElement("div");
	bCol.classList.add("col");
	bCol.appendChild(bCon);
	var graphCont = document.createElement("div");
	graphCont.id = "graph-" + index;
	var graphCol = document.createElement("div");
	graphCol.classList.add("col");
	graphCol.appendChild(graphCont);
	var row = document.createElement("div");
	row.classList.add("row");
	row.id = "row-" + index;
	row.appendChild(graphCol);
	row.appendChild(bCol);
	mainGraph.container.appendChild(row);

	contractButton.onclick = function(e) {
	// Get the graph
	// Send to endpoint
	// await? results

	// create new things
	createRow(index+1);
	}
}
